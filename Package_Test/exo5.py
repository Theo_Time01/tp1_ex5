#!/usr/bin/env python
# coding: utf-8
"""
author : theotime Perrichet

-Ce code permet de tester les méthodes de la classe SimpleComplexCalculator
du package Package_Calculator

"""
from __future__ import print_function

#importation de la classe via le package
from Package_Calculator.Calculator import SimpleComplexCalculator



#tests
TEST = SimpleComplexCalculator()
print(TEST.sum_complex([10, 1], [10, 5]))
print(TEST.sum_complex([10, 1], [10, "5"]))
print(TEST.substract_complex([10, 1], [10, 5]))
print(TEST.substract_complex([10, 1], [10, "5"]))
print(TEST.multiply_complex([10, 1], [10, 5]))
print(TEST.multiply_complex([10, 1], [10, "5"]))
print(TEST.divide_complex([10, 1], [10, 5]))
print(TEST.divide_complex([10, 1], [10, 0]))
print(TEST.divide_complex([10, 1], [10, "5"]))

