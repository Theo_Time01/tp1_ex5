# TP1 Exercice 5

## Objectif 
L'objectif de cet exercice est de tester dans les méthodes de calcul, implémenter dans des packages, (cf: TP1 Exercice 4) que les entrées sont biens des entiers, on renvoit "ERROR" si le calcul est impossible

## Realisation 
J'ai créé deux packages, un permettant de stocker le script de SimpleCalculator et un autre package contenant un script de test.

## Organisation
voici l'arborescence de notre projet :

    .
    ├── Package_Calculator
    │   ├── Calculator.py
    │   ├── __init__.py
    │   └── __pycache__
    │   
    │   
    ├── Package_Test
    │   ├── exo5.py
    │   └── __init__.py
    └── README.md




## Lancement

On lance la commande :  
  
    export PYTHONPATH=$PYTHONPATH:'.'

puis on execute la commande :  

    python Package_Test/exo5.py
